from browserstack.local import Local
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# creates an instance of Local
bs_local = Local()

# replace <browserstack-accesskey> with your key. You can also set an environment variable - "BROWSERSTACK_ACCESS_KEY".
bs_local_args = {"key": "<Your API Key>", "force": "true",
                 "v": "true", "forcelocal": "true", "onlyAutomate": "true", "proxyHost": "<Your proxy host>", "proxyPort": "<Your proxy port>"}

# starts the Local instance with the required arguments
bs_local.start(**bs_local_args)

# check if BrowserStack local instance is running
print(bs_local.isRunning())

BROWSERSTACK_URL = 'https://<BrowserStackusername:API Key>@hub-cloud.browserstack.com/wd/hub'

desired_cap = {

    'os': 'Windows',
    'os_version': '10',
    'browser': 'Chrome',
    'browser_version': '80',
    'name': "<Test name>",
    'browserstack.local': "true",
    'browserstack.networkLogs': "true"

}

driver = webdriver.Remote(
    command_executor=BROWSERSTACK_URL,
    desired_capabilities=desired_cap
)

# your Selenium test here

# # stop the Local instance
bs_local.stop()
