# Browserstack-local demo

following https://www.browserstack.com/local-testing/automate tutorial

1. Fill in your test
2. Fill in your username and API key
3. Use the VS Code dev container or run it in python3 

## Python3 way:

```
$ pip3 install --user -r requirements.txt
$ ./test.py
```

